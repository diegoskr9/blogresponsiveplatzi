/* FORMA 1

const mymenu = document.querySelector('.nav-container')

const burgerButton = document.querySelector('#burger-menu')

burgerButton.addEventListener('click', hideShowMenu)

function hideShowMenu() {
    if (mymenu.classList.contains('nav-is-active')) {
        mymenu.classList.remove('nav-is-active')
    } else {
        mymenu.classList.add('nav-is-active')
    }

} */


//FORMA 2

const burgerButton = document.querySelector('#burger-menu')

const linksSections = document.querySelectorAll('.nav-main li a')
    // console.log(linksSections)

const ipad = window.matchMedia("(max-width:767px)")

// loadButtonBurger()

ipad.addEventListener('change', validateMediaQuerie)
validateMediaQuerie(ipad)

function validateMediaQuerie(event) { //event, en realidad puede ser cualquier nombre que queramos.
    if (event.matches) {
        burgerButton.addEventListener('click', showmenu)
        Array.from(linksSections).forEach(link => {
            link.addEventListener('click', showmenu)
        })
    } else {
        burgerButton.removeEventListener('click', showmenu)
    }
}


function showmenu() {
    const mymenu = document.querySelector('.nav-container')
    mymenu.classList.toggle('nav-is-active')
}


/* function loadButtonBurger() {
    if (ipad.matches) {
        burgerButton.addEventListener('click', showmenu)
    }
} */